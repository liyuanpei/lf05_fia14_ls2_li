public class Sparbuch {
    private int kontonummer;
    private double kapital;
    private double zinssatz;

    public int getKontonummer() {
        return kontonummer;
    }

    public void setKontonummer(int kontonummer) {
        this.kontonummer = kontonummer;
    }

    public double getKapital() {
        return kapital;
    }

    public void setKapital(int kapital) {
        this.kapital = kapital;
    }

    public double getZinssatz() {
        return zinssatz;
    }

    public void setZinssatz(int zinssatz) {
        this.zinssatz = zinssatz;
    }

    public Sparbuch() {
    }

    public Sparbuch(int kontonummer, int kapital, double zinssatz) {
        this.kontonummer = kontonummer;
        this.kapital = kapital;
        this.zinssatz = zinssatz/100;
    }

    void zahleEin(int einzahlsumme) {
        this.kapital += einzahlsumme;
    }

    void hebeAb(int abhebeSumme) {
        this.kapital -= abhebeSumme;
    }

    double getErtrag(int zeitraum){
        for (int i = 0; i < zeitraum; i++) {
            verzinse();
        }
        return this.kapital;
    }

    void verzinse(){
        kapital *= 1 + zinssatz;
    }
}
