public class StarteWeltraum {

    public static void main(String[] args) {
        Ladung ladung1 = new Ladung("Ferengi Schneckensaft", 200);
        Ladung ladung2 = new Ladung("Borg-Schrott", 5);
        Ladung ladung3 = new Ladung("Rote Materie", 2);
        Ladung ladung4 = new Ladung("Forschungssonde", 35);
        Ladung ladung5 = new Ladung("Bat'leth Klingonen Schwert", 200);
        Ladung ladung6 = new Ladung("Plasma-Waffe", 50);
        Ladung ladung7 = new Ladung("Photonentorpedo", 3);

        Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
        Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
        Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");

        klingonen.addLadung(ladung1);
        klingonen.addLadung(ladung5);

        romulaner.addLadung(ladung2);
        romulaner.addLadung(ladung3);
        romulaner.addLadung(ladung6);

        vulkanier.addLadung(ladung4);
        vulkanier.addLadung(ladung7);

        klingonen.photonentorpedoSchiessen(romulaner);
        romulaner.phaserkanoneSchiessen(klingonen);
        vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
        klingonen.zustandRaumschiff();
        klingonen.ladungsverzeichnisAusgeben();
        vulkanier.reperaturDurchfuehren(true, true, true, vulkanier.getAndroidenAnzahl());
        vulkanier.photonentorpedosLaden(3);
        vulkanier.ladungsverzeichnisAufraeumen();
        klingonen.photonentorpedoSchiessen(romulaner);
        klingonen.photonentorpedoSchiessen(romulaner);

        System.out.println("Kampf vorbei, Bericht folgt...");

        klingonen.zustandRaumschiff();
        klingonen.ladungsverzeichnisAusgeben();
        vulkanier.zustandRaumschiff();
        vulkanier.ladungsverzeichnisAusgeben();
        romulaner.zustandRaumschiff();
        romulaner.ladungsverzeichnisAusgeben();

        System.out.println("---------------LOGBUCH------------------");
        for (String str : Raumschiff.eintraegeLogbuchZurueckgeben()) {
            System.out.println(str);
        }
    }
}
