import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/** Repräsentiert ein Raumschiff
 * @author Richard Li
 */
public class Raumschiff {

    private int photonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int schildeInProzent;
    private int huelleInProzent;
    private int lebenserhaltungssystemeInProzent;
    private int androidenAnzahl;
    private String schiffsname;
    /**
     * Repräsentiert die Kommunikation von einem Schiff
     */
    private ArrayList<String> broadcastKommunikator;

    /**
     * Repräsentiert das Ladungsverzeichnis vom Schiff
     */
    private ArrayList<Ladung> ladungsverzeichnis;

    /**
     * Repräsentiert das Logbuch von der Kommunikation ALLER Raumschiffe
     */
    public static ArrayList<String> logbuch = new ArrayList<>();

    /**
     * Erzeugt ein Raumschiff mit Standardwerten.
     */
    public Raumschiff() {
        this.photonentorpedoAnzahl = 100;
        this.energieversorgungInProzent = 100;
        this.schildeInProzent = 100;
        this.huelleInProzent = 100;
        this.lebenserhaltungssystemeInProzent = 100;
        this.androidenAnzahl = 10;
        this.schiffsname = "Default Name";
        this.broadcastKommunikator = new ArrayList<>();
        this.ladungsverzeichnis = new ArrayList<>();
    }

    /**
     * Erzeugt ein Raumschiff mit folgenden werten:
     * @param photonentorpedoAnzahl Die Anzahl an Torpedos.
     * @param energieversorgungInProzent Der Zustand der Energieversorgung.
     * @param schildeInProzent Der Zustand der Schilde.
     * @param huelleInProzent Der Zustand der Hülle.
     * @param lebenserhaltungssystemeInProzent Der Zustand des Lebenserhaltungssystems.
     * @param androidenAnzahl Die Anzahl der Reperatur-Androiden
     * @param schiffsname Der Name des Raumschiffs
     */
    public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent,
                      int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
        this();
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
        this.energieversorgungInProzent = energieversorgungInProzent;
        this.schildeInProzent = schildeInProzent;
        this.huelleInProzent = huelleInProzent;
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
        this.androidenAnzahl = androidenAnzahl;
        this.schiffsname = schiffsname;
    }

    /**
     * Fügt eine neue Ladung zum Raumschiff hinzu.
     * @param neueLadung Die Ladung, die dem Raumschiff hinzugefügt werden soll, Objekt der Klasse 'Ladung'.
     */
    public void addLadung(Ladung neueLadung) {
        ladungsverzeichnis.add(neueLadung);
    }

    /**
     * Schießt einen Torpedo auf das gewünschte Ziel Raumschiff ab.
     * @param ziel Das Ziel Raumschiff.
     */
    public void photonentorpedoSchiessen(Raumschiff ziel) {
        if (photonentorpedoAnzahl == 0) {
            nachrichtAnAlle("-=*Click*=-");
        } else {
            photonentorpedoAnzahl--;
            nachrichtAnAlle("Photonentorpedo abgeschossen");
            ziel.treffer(this);
        }
    }

    /**
     * Schießt mit der Phaserkanone auf das gewünschte Ziel Raumschiff ab.
     * @param ziel Das Ziel Raumschiff.
     */
    public void phaserkanoneSchiessen(Raumschiff ziel) {
        if (energieversorgungInProzent < 50) {
            nachrichtAnAlle("-=*Click*=-");
        } else {
            energieversorgungInProzent = energieversorgungInProzent - 50;
            nachrichtAnAlle("Phaserkanone abgeschossen");
            ziel.treffer(this);
        }
    }


    /**
     * Bei einem Treffer wird diese Methode vom getroffenen Raumschiff aufgerufen.
     * @param angreifer Das Raumschiff, von dem der Angriff ausgeführt wurde.
     */
    private void treffer(Raumschiff angreifer) {
        System.out.println(this.schiffsname + " wurde von " + angreifer.schiffsname + " getroffen!");
        schildeInProzent = schildeInProzent - 50;
        if (schildeInProzent <= 0) {
            huelleInProzent = huelleInProzent - 50;
            energieversorgungInProzent = energieversorgungInProzent - 50;
            if (huelleInProzent <= 0) {
                lebenserhaltungssystemeInProzent = 0;
                nachrichtAnAlle("Die Lebenserhaltungssysteme von " + this.schiffsname + "wurden zerstört.");
            }
        }
    }

    /**
     * Nachricht an alle Raumschiffe schicken. Unter anderem wird hier gleichzeitig in das Logbuch geschrieben.
     * @param message Der Nachrichteninhalt, der verschickt werden soll.
     */
    public void nachrichtAnAlle(String message) {
        broadcastKommunikator.add(message);
        logbuch.add(schiffsname + " sagt: " + message);
    }

    /**
     * Zurückgeben des Logbuchs
     * @return Gibt das Logbuch zurück.
     */
    public static ArrayList<String> eintraegeLogbuchZurueckgeben() {
        return logbuch;
    }

    /**
     * Nachladen einer gewünschten Anzahl an Torpedos.
     * @param anzahlTorpedos Die Menge an Torpedos, die nachgeladen werden sollen.
     */
    public void photonentorpedosLaden(int anzahlTorpedos) {
        boolean torpedosVorhanden = false;
        Ladung torpedoLadung = new Ladung();
        for (Ladung aktuelleLadung : ladungsverzeichnis) {
            if (aktuelleLadung.getBezeichnung().equals("Photonentorpedo")) {
                torpedosVorhanden = true;
                torpedoLadung = aktuelleLadung;
            }
        }
        if (!torpedosVorhanden) {
            System.out.println("Keine Photonentorpedos gefunden!");
            nachrichtAnAlle("-=*Click*=-");
        } else {
            if (anzahlTorpedos > torpedoLadung.getMenge()) {
                photonentorpedoAnzahl = photonentorpedoAnzahl + torpedoLadung.getMenge();
                System.out.println(schiffsname + ": " + torpedoLadung.getMenge() + " Photonentorpedo(s) eingesetzt");
                torpedoLadung.setMenge(0);
            } else {
                photonentorpedoAnzahl = photonentorpedoAnzahl + anzahlTorpedos;
                torpedoLadung.setMenge(torpedoLadung.getMenge() - anzahlTorpedos);
                System.out.println(schiffsname + ": " + anzahlTorpedos + " Photonentorpedo(s) eingesetzt");
            }
        }
    }

    /**
     * Alle Ladungseinträge die keine Mengen haben, werden entfernt.
     */
    public void ladungsverzeichnisAufraeumen() {
        Iterator<Ladung> itr = ladungsverzeichnis.iterator();
        while (itr.hasNext()) {
            int menge = itr.next().getMenge();
            if (menge <= 0) {
                itr.remove();
            }
        }
    }

    /**
     * Methode zur Erzeugung einer zufälligen Zahl.
     * @return Gibt eine zufällige ganze Zahl zurück.
     */
    public static int zufallsZahl() {
        Random rnd = new Random();
        return rnd.nextInt(100) + 1;
    }

    /**
     * Schiffsteile werden hier repariert.
     * @param schild Ob das Schild repariert werden soll.
     * @param energieversorgung Ob die Energieversorgung repariert werden soll.
     * @param huelle Ob die Hülle repariert werden soll.
     * @param anzahlAndroiden Die Anzahl der Androiden, mit der repariert werden soll.
     */
    public void reperaturDurchfuehren(boolean schild, boolean energieversorgung, boolean huelle, int anzahlAndroiden) {
        if (androidenAnzahl < anzahlAndroiden) {
            anzahlAndroiden = androidenAnzahl;
        }
        int zufallszahl = zufallsZahl();
        int anzahlTrue = 0;
        if (schild) {
            anzahlTrue++;
        }

        if (energieversorgung) {
            anzahlTrue++;
        }

        if (huelle) {
            anzahlTrue++;
        }

        int reperaturwerte = (zufallszahl * anzahlAndroiden) / anzahlTrue;

        if (schild) {
            schildeInProzent = schildeInProzent + reperaturwerte;
        }

        if (energieversorgung) {
            energieversorgungInProzent = energieversorgungInProzent + reperaturwerte;
        }

        if (huelle) {
            huelleInProzent = huelleInProzent + reperaturwerte;
        }
    }

    /**
     * Ausgabe des Ladungsverzeichnes des Raumschiffes.
     */
    public void ladungsverzeichnisAusgeben() {
        System.out.println("Ladungsverzeichnis für " + schiffsname);
        for (Ladung ladung : ladungsverzeichnis) {
            System.out.println(ladung.getBezeichnung() + " : " + ladung.getMenge());
        }
    }

    /**
     * Ausgabe des Zustand des Raumschiffes.
     */
    public void zustandRaumschiff() {
        System.out.println();
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("Schiffsname: " + schiffsname);
        System.out.println("Photonentorpedo Anzahl = " + photonentorpedoAnzahl);
        System.out.println("Energieversorgung In Prozent = " + energieversorgungInProzent);
        System.out.println("Schilde In Prozent = " + schildeInProzent);
        System.out.println("Huelle In Prozent = " + huelleInProzent);
        System.out.println("Lebenserhaltungssysteme In Prozent = " + lebenserhaltungssystemeInProzent);
        System.out.println("Androiden Anzahl = " + androidenAnzahl);
        System.out.println("---------------------------------------------------------------------------");
        System.out.println();
    }

    public int getPhotonentorpedoAnzahl() {
        return photonentorpedoAnzahl;
    }

    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
    }

    public int getEnergieversorgungInProzent() {
        return energieversorgungInProzent;
    }

    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
        this.energieversorgungInProzent = energieversorgungInProzent;
    }

    public int getSchildeInProzent() {
        return schildeInProzent;
    }

    public void setSchildeInProzent(int schildeInProzent) {
        this.schildeInProzent = schildeInProzent;
    }

    public int getHuelleInProzent() {
        return huelleInProzent;
    }

    public void setHuelleInProzent(int huelleInProzent) {
        this.huelleInProzent = huelleInProzent;
    }

    public int getLebenserhaltungssystemeInProzent() {
        return lebenserhaltungssystemeInProzent;
    }

    public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
    }

    public int getAndroidenAnzahl() {
        return androidenAnzahl;
    }

    public void setAndroidenAnzahl(int androidenAnzahl) {
        this.androidenAnzahl = androidenAnzahl;
    }

    public String getSchiffsname() {
        return schiffsname;
    }

    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }

    public ArrayList<String> getBroadcastKommunikator() {
        return broadcastKommunikator;
    }

    public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
        this.broadcastKommunikator = broadcastKommunikator;
    }

    public ArrayList<Ladung> getLadungsverzeichnis() {
        return ladungsverzeichnis;
    }

    public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
        this.ladungsverzeichnis = ladungsverzeichnis;
    }
}
