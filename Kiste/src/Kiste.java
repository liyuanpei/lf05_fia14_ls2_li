public class Kiste {
    int hoehe;
    int breite;
    int tiefe;
    String farbe;

    public Kiste() {
    }

    public Kiste(int hoehe, int breite, int tiefe, String farbe) {
        this.hoehe = hoehe;
        this.breite = breite;
        this.tiefe = tiefe;
        this.farbe = farbe;
    }

    public int getHoehe() {
        return hoehe;
    }

    public void setHoehe(int hoehe) {
        this.hoehe = hoehe;
    }

    public int getBreite() {
        return breite;
    }

    public void setBreite(int breite) {
        this.breite = breite;
    }

    public int getTiefe() {
        return tiefe;
    }

    public void setTiefe(int tiefe) {
        this.tiefe = tiefe;
    }

    public String getFarbe() {
        return farbe;
    }

    public void setFarbe(String farbe) {
        this.farbe = farbe;
    }

    public int getVolumen(){
        return hoehe * breite * tiefe;
    }
}
