public class Main {

    public static void main(String[] args) {
        Kiste eins = new Kiste(10, 10, 10, "schwarz");
        Kiste zwei = new Kiste(5, 40, 20, "weiß");
        Kiste drei = new Kiste(20, 30, 50, "rot");

        System.out.println("Das Volumen von Kiste eins ist: " +  eins.getVolumen());
        System.out.println("Die Farbe von Kiste eins ist: " +  eins.getFarbe());
        System.out.println();
        System.out.println("Das Volumen von Kiste zwei ist: " +  zwei.getVolumen());
        System.out.println("Die Farbe von Kiste zwei ist: " +  zwei.getFarbe());
        System.out.println();
        System.out.println("Das Volumen von Kiste drei ist: " +  drei.getVolumen());
        System.out.println("Die Farbe von Kiste drei ist: " +  drei.getFarbe());
    }
}