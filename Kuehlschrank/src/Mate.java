public class Mate extends Getraenke {

    public Mate(String name) {
        super(name, 500);
    }

    @Override
    public boolean trinken() {
        if (menge - 100 < 0) {
            menge = 0;
            return false;
        }
        menge = menge - 100;
        return true;
    }

    @Override
    public String status() {
        return " Klasse: Mate, Instanz: " + this.name + ", Menge: " + this.menge + "ml";
    }
}
