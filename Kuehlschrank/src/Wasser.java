public class Wasser extends Getraenke {

    public Wasser(String name, int menge) {
        super(name, menge);
    }

    @Override
    public boolean trinken() {
        if (menge - 200 < 0) {
            menge = 0;
            return false;
        }
        menge = menge - 200;
        return true;
    }

    @Override
    public String status() {
        return " Klasse: Wasser, Instanz: " + this.name + ", Menge: " + this.menge + "ml";
    }
}
