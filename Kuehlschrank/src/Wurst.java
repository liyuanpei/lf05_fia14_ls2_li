public class Wurst extends Speise {

    public Wurst(String name, int menge) {
        super(name, menge);
    }

    @Override
    public boolean essen() {
        if (menge - 10 < 0) {
            menge = 0;
            return false;
        }
        menge = menge - 10;
        return true;
    }

    @Override
    public String status() {
        return " Klasse: Wurst, Instanz: " + this.name + ", Menge: " + this.menge + "g";
    }
}
