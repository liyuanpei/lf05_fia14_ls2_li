public class Brot extends Speise {

    public Brot(int typ, int menge) {
        super("", menge);
        switch (typ) {
            case 0 -> this.name = "Weißbrot";
            case 1 -> this.name = "Schwarzbrot";
            case 2 -> this.name = "Mischbrot";
            default -> this.name = "Spezialbrot";
        }

        //switch (typ){
        //            case 0:
        //                this.name = "Weißbrot";
        //                break;
        //            case 1:
        //                this.name = "Schwarzbrot";
        //                break;
        //            case 2:
        //                this.name = "Mischbrot";
        //                break;
        //            default:
        //                this.name = "Spezialbrot";
        //                break;
        //        }

    }

    @Override
    public boolean essen() {
        if (menge - 50 < 0) {
            menge = 0;
            return false;
        }
        menge = menge - 50;
        return true;
    }

    @Override
    public String status() {
        return " Klasse: Brot, Instanz: " + this.name + ", Menge: " + this.menge + "g";
    }
}
